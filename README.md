Little utility to automate creating `tar.br` files

## Build

install the brotli dev dependency for your distribution listed below.

install [go](https://golang.org/doc/install)

clone the repo:

```bash
git clone https://gitlab.com/reedrichards/tarbr
cd tarbr
```

build:

```bash
go build
```

install:

```bash
go install
```

usage:

```bash
tarbr foo
```

doesn't have any other features at the moment but open an issue if you'd like me to extend it.

## Dependency

Debian/Ubuntu

```sh
sudo apt-get install libbrotli-dev
```

RHEL/CentOS

```sh
yum install brotli-devel
```

Alpine

```sh
apk add brotli-dev
```

MacOS (with [homebrew](https://brew.sh))

```sh
brew install brotli
```

Windows (with [msys2](https://www.msys2.org))

```sh
pacman -S mingw-w64-x86_64-brotli
```
